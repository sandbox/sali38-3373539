# DSFR CORE

Provides services for other modules of the DSFR kickstart distribution.

## DSFR theme required!

https://www.drupal.org/project/dsfr

This module will test whether the parent theme is active.

If you use the "/admin/dsfr/theme" link, you'll be redirected to the parent theme (if active).

## Install with Drush

```shell
drush en dsfr_core -y
```

This module has the following dependencies:
* https://www.drupal.org/project/form_options_attributes
* https://www.drupal.org/project/style_selector

## DSFR Kickstart

Ce module est inclus dans cette distribution, mais n'a besoin que de l'activitation du thème DSFR pour fonctionner.

https://www.drupal.org/project/dsfr_kickstart

## Children's modules available

This module will test which child modules are active.

* https://www.drupal.org/project/dsfr_block
* https://www.drupal.org/project/dsfr_menu
* https://www.drupal.org/project/dsfr_paragraph

## Module related to this project

https://www.drupal.org/project/dsfr_twig_components

## Author

Code created by Jérôme Bouquet (https://www.drupal.org/u/netprogfr)