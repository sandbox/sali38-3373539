<?php

namespace Drupal\dsfr_core;

use Drupal\Core\Controller\ControllerBase;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Provides field associations to entities, 
 * such as blocks
 */
class FieldManage extends ControllerBase {

  /**
   * Checks the field storage for a given entity type.
   *
   * @param string $entity_type
   *   The entity type to check.
   *
   * @return string
   *   HTML markup showing the status of field storage.
   */
  public function checkFieldsStorage( string $entity_type ): string {

    $storage = \Drupal::service('dsfr_core.fieldStorage')->fieldData();

    $fields_storage='<h2>DSFR : Fields storage ('. $entity_type .')</h2><ul>';
    foreach( $storage as $field_name => $conf ) {

      $is_ready = $this->t('To be installed');
      $field_storage = FieldStorageConfig::loadByName( $entity_type, $field_name);

      if( !empty( $field_storage ) ) { $is_ready = $this->t('installed'); } 

      $fields_storage.= "<li> $field_name (". $is_ready .")</li>";
    }
    $fields_storage.='</ul>';

    return $fields_storage;
  }

  /**
   * Installs field storage for a given entity type.
   *
   * @param string $entity_type
   *   The entity type to install field storage for.
   */
  public function installFieldsStorage( string $entity_type ): void {

    $storage = \Drupal::service('dsfr_core.fieldStorage')->fieldData();

    foreach( $storage as $field_name => $conf ) {

      $field_storage = FieldStorageConfig::loadByName( $entity_type, $field_name );

      if( empty( $field_storage ) ) { 
        
        $this->createFieldsStorage( 
          $field_name, 
          $conf, 
          $entity_type ); 
      }
    }
  }

  /**
   * Creates field storage for a given field.
   *
   * @param string $field_name
   *   The name of the field.
   * @param array $conf
   *   The configuration array for the field.
   * @param string $entity_type
   *   The entity type for the field.
   *
   * @return \Drupal\field\Entity\FieldStorageConfig
   *   The created field storage configuration.
   */
  public function createFieldsStorage(
    string $field_name,
    array $conf,
    string $entity_type = 'block_content'
  ): FieldStorageConfig {

    $module = 'core';
    $settings = [];
    extract($conf);

    $field_config_storage = [

      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'type' => $type,

      'module' => $module,
      'settings' => $settings,

      'langcode' => 'en',
      'translatable' => true,
      'status' => true,
      'locked' => false,
      'indexes' => [],
      'persist_with_no_fields' => false,
      'custom_storage' => false,

      'format' => 'restricted_html_dsfr'
    ];
    //if( isset($format) ) { $field_config_storage['format'] = $format; }

    $field_storage = FieldStorageConfig::create($field_config_storage);
    $field_storage->save();

    return $field_storage;
  }

  /**
   * Creates a field instance.
   *
   * @param array $conf
   *   The configuration array for the field.
   * @param \Drupal\field\Entity\FieldStorageConfig $field_storage
   *   The field storage configuration.
   * @param string $bundle
   *   The bundle to attach the field to.
   *
   * @return \Drupal\field\Entity\FieldConfig
   *   The created field configuration.
   */
  public function createFields(
    array $conf,
    FieldStorageConfig $field_storage,
    string $bundle
  ): FieldConfig {

    $required = false;
    $settings = $default_value = [];
    $description = $default_value_callback = '';

    extract($conf);

    $field_config = [

      'field_storage' => $field_storage,
      'bundle' => $bundle,
      'label' => $label,
      'description' => $description,
      
      'required' => $required,

      'langcode' => 'en',
      'translatable' => true,
      'status' => true,

      'default_value' => $default_value,
      'default_value_callback' => $default_value_callback,

      'settings' => $settings,
    ];

    //if( isset($format) ) { $field_config['format'] = $format; }

    $field = FieldConfig::create($field_config);
    $field->save();
  
    return $field;
  }

  /**
   * Sets the form display for a field.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_id
   *   The entity ID.
   * @param string $field_name
   *   The name of the field.
   * @param string $form_display_type
   *   The form display type.
   */
  public function displayFields(
    string $entity_type,
    string $entity_id,
    string $field_name,
    ?string $form_display_type
  ): void {

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    // Assign widget settings for the default form mode.
    $display_repository->getFormDisplay($entity_type, $entity_id)
      ->setComponent( $field_name, [
        'type' => $form_display_type
      ])
      ->save();
  }

  /**
   * Sets the VIEW display for a field.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_id
   *   The entity ID.
   * @param string $field_name
   *   The name of the field.
   * @param string $view_display_type
   *   The view display type.
   * @param array $view_display_settings
   *   The view display settings.
   * @param string $label_show
   *   How to show the label.
   */
  public function viewFields(
    string $entity_type,
    string $entity_id,
    string $field_name,
    ?string $view_display_type,
    array $view_display_settings = [],
    string $label_show = 'hidden'
  ): void {

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    /* Label show available:
      - above
      - inline
      - hidden
      - visually_hidden
    */

    $view = [
      'label' => $label_show,
      'type' => $view_display_type,
    ];

    if ( count($view_display_settings) > 0 ) { $view['settings'] = $view_display_settings; } 

    // Assign display settings for default view mode.
    $display_repository->getViewDisplay( $entity_type, $entity_id )
    ->setComponent( $field_name, $view)
    ->save();

    /* Allowed type:
    - comment_permalink, 
    - comment_default, 
    - comment_username, 
    - datetime_plain, 
    - datetime_default, 
    - datetime_custom, 
    - datetime_time_ago, 
    - entity_reference_revisions_entity_view, 
    - file_rss_enclosure, 
    - file_link, 
    - file_url_plain, 
    - file_video, 
    - file_table, 
    - file_default, 
    - file_filemime, 
    - file_audio, 
    - file_size, 
    - file_extension, 
    - file_uri, 
    - image, 
    - image_url, 
    - layout_paragraphs_builder, 
    - layout_paragraphs, 
    - link, 
    - link_separate, 
    - oembed, 
    - media_thumbnail, 
    - list_key, 
    - list_default, 
    - responsive_image, 
    - entity_reference_rss_category, 
    - telephone_link, 
    - text_default, 
    - text_trimmed, 
    - text_summary_or_trimmed, 
    - user_name, 
    - author, 
    - paragraph_summary, 
    - entity_reference_entity_view, 
    - language, 
    - email_mailto, 
    - basic_string, 
    - number_integer, 
    - number_unformatted, 
    - entity_reference_label, 
    - timestamp_ago, 
    - boolean, 
    - timestamp, 
    - string, 
    - uri_link, 
    - number_decimal, 
    - entity_reference_entity_id
    */
  }
}
?>