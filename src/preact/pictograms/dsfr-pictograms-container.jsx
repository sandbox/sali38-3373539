import { render } from 'preact';
import { App } from './app.jsx';
import '../../style/pictograms_list.css';

(function (Drupal) {

  'use strict';

  // FIX conflict with defer
  if (typeof _.defer !== 'function') {
    _.defer = function(func) {
      return setTimeout(func, 0);
    };
  }
  
  Drupal.behaviors.dsfrPictogramsList = {
    attach: function (context, settings) {

      once('dsfrPictogramsListIdentifier', 'body', context).forEach(function (element) {

        const app = document.getElementById('dsfr-pictograms-container')
        const appProps = {
          pathDsfr: app.dataset.dsfr, 
          initialSearchTerm: app.dataset.search,
          language: app.dataset.language
        };
      
        render( <App {...appProps} />, app );
      });
    }
  };
})(Drupal);