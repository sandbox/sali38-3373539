import { CodeBlock } from './code.jsx';
import { PictogramSvg } from './pictogram-svg.jsx';

export const PictogramModal = ({ pictogramsPath, name, type, labels }) => (

  <dialog aria-labelledby={`fr-modal-${type}-${name}-title`} id={`fr-modal-${type}-${name}`} class="fr-modal" role="dialog" >
    <div class="fr-container fr-container--fluid fr-container-md">
      <div class="fr-grid-row fr-grid-row--center">
        <div class="fr-col-12 fr-col-md-8 fr-col-lg-6">
          <div class="fr-modal__body">
            <div class="fr-modal__header">
              <button class="fr-btn--close fr-btn" aria-controls={`fr-modal-${type}-${name}`}>{labels.close}</button>
            </div>
            <div class="fr-modal__content">
              <PictogramSvg pictogramsPath={pictogramsPath} type={type} name={name} />
              <div>{name} <p class="fr-tag fr-tag--sm fr-ml-1w">{type}</p></div>                  
              <CodeBlock pictogramsPath={pictogramsPath} name={name} type={type} labels={labels} />
            </div>
          </div>
        </div>
      </div>
    </div>
  </dialog>
)