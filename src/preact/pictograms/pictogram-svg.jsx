export const PictogramSvg = ({ pictogramsPath, type, name  }) => {

  const pictoPath = `${pictogramsPath}/${type}/${name}`;

  return (
    <>
      <svg aria-hidden="true" class="fr-artwork" viewBox="0 0 80 80" width="80px" height="80px">
        <use class="fr-artwork-decorative" href={`${pictoPath}.svg#artwork-decorative`}></use>
        <use class="fr-artwork-minor" href={`${pictoPath}.svg#artwork-minor`}></use>
        <use class="fr-artwork-major" href={`${pictoPath}.svg#artwork-major`}></use>
      </svg>  
    </>
  )  
}
