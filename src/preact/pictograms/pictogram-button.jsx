import { PictogramSvg } from './pictogram-svg.jsx';

export const PictogramButton = ({ pictogramsPath, type, name }) => (

  <button 
    class="fr-btn fr-btn--tertiary-no-outline"
    data-fr-opened="false" 
    aria-controls={`fr-modal-${type}-${name}`}
  >
    <PictogramSvg pictogramsPath={pictogramsPath} type={type} name={name} />
    <span class="fr-text--sm fr-ml-1v drupal-pictogram-item">{name}</span>
  </button>
  
)