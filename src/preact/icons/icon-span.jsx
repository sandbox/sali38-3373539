export const IconSpan = ({ name, option = '' }) => (
  <span class={`fr-icon-${name}${option}`} aria-hidden="true"></span>
)