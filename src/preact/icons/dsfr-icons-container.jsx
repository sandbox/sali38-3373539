import { render } from 'preact';
import { App } from './app.jsx';
import '../../style/icons_list.css';

(function (Drupal) {

  'use strict';

  // FIX conflict with defer
  if (typeof _.defer !== 'function') {
    _.defer = function(func) {
      return setTimeout(func, 0);
    };
  }
  
  Drupal.behaviors.dsfrIconsList = {
    attach: function (context, settings) {

      once('dsfrIconsListIdentifier', 'body', context).forEach(function (element) {

        const app = document.getElementById('dsfr-icons-container')
        const appProps = {
          initialSearchTerm: app.dataset.search,
          language: app.dataset.language
        };
      
        render( <App {...appProps} />, app );

      });
    }
  };
})(Drupal);