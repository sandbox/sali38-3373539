import { useState, useEffect, useCallback, useMemo } from 'preact/hooks';
import { Form } from '../commons/form.jsx';
import { IconButton } from './icon-button.jsx';
import { IconModal } from './icon-modal.jsx';
import { Pagination } from '../commons/pagination.jsx';
import { validateNameFilter } from '../commons/valid-name.jsx';

export const IconsList = ({ iconsData, initialSearchTerm, labels }) => {

  const [nameFilter, setNameFilter] = useState(validateNameFilter( initialSearchTerm ));
  const [typeFilter, setTypeFilter] = useState('all');

  const [currentPage, setCurrentPage] = useState(1);
  const iconsPerPage = 40;

  // ------------------------------------------------------------------------------ //
  // Listing & filter controller

  const handleNameFilterChange = useCallback((e) => {
    const newValue = validateNameFilter(e.target.value);
    setNameFilter(newValue);
  }, []);

  const handleTypeFilterChange = useCallback((e) => {
    setTypeFilter(e.target.value);
    setCurrentPage(1);
  }, []);

  const allIcons = useMemo(() => {
    return Object.entries(iconsData)
      .flatMap(([type, icons]) => 
        icons.map(icon => ({ name: icon, type }))
      )
      .sort((a, b) => a.name.localeCompare(b.name));
  }, [iconsData]);

  const types = useMemo(() => {
    return ['all', ...Object.keys(iconsData)];
  }, [iconsData]);

  const filteredIcons = useMemo(() => {
    return allIcons.filter(icon => 
      icon.name.toLowerCase().includes(nameFilter.toLowerCase()) &&
      (typeFilter === 'all' || icon.type === typeFilter)
    );
  }, [allIcons, nameFilter, typeFilter]);

  const formProps = {
    nameFilter,
    handleNameFilterChange, 
    typeFilter,
    handleTypeFilterChange,
    types,
    labels
  }

  // ------------------------------------------------------------------------------ //
  // Pages controller
  
  useEffect(() => {
    setCurrentPage(1);
  }, [nameFilter, typeFilter]);

  const totalPages = Math.max(1, Math.ceil(filteredIcons.length / iconsPerPage));
  const hasResults = filteredIcons.length > 0;

  const currentIcons = useMemo(() => {
    const startIndex = (currentPage - 1) * iconsPerPage;
    return filteredIcons.slice(startIndex, startIndex + iconsPerPage);
  }, [filteredIcons, currentPage]);


  const paginationProps = {
    nameFilter,
    typeFilter,
    currentPage,
    setCurrentPage,
    hasResults,
    totalPages,
    labels
  };

  // ------------------------------------------------------------------------------ //

  return (
    <>
      <Form {...formProps} />

      {/* Listing (with pagination) */}
      <p class="fr-mt-3v">
        <strong>{filteredIcons.length} {labels.item}(s)</strong> | {labels.see}:&nbsp; 
        <a class="fr-link" href="https://www.systeme-de-design.gouv.fr/fondamentaux/icone" target="_blank" rel="noopener external">{labels.official}</a> -&nbsp;
        <a class="fr-link" href="https://remixicon.com/" target="_blank" rel="noopener external">Remixicon</a>
      </p>
      <div class="fr-grid-row fr-mt-9v">
        {currentIcons.map(icon => (
          <div key={icon.name} class="fr-col-12 fr-col-lg-3 fr-mb-3v">
            <IconButton name={icon.name} />
            <IconModal name={icon.name} type={icon.type} labels={labels} /> 
          </div>
        ))}
      </div>
      <div>
        <Pagination {...paginationProps} />      
      </div>
    </>
  );
};