import { capitalizeFirstLetter } from "./capitalize-first-letter"

export const Form = ({ 
  nameFilter,
  handleNameFilterChange, 
  typeFilter,
  handleTypeFilterChange,
  types,
  labels
}) => (

  <div class="fr-grid-row fr-grid-row--gutters">
    <div class="fr-col-12 fr-col-lg-9">  
      <div class="fr-input-group">
        <label class="fr-label" for="text-input-text">{labels.name}</label>
        <div class="fr-input-wrap fr-icon-search-line">
          <input 
            type="text" 
            class="fr-input" 
            id="text-input-text"
            name="text-input-text"
            placeholder={labels.placeholder}
            value={nameFilter}
            onInput={handleNameFilterChange}
          />
        </div>
      </div>
    </div>
    <div class="fr-col-12 fr-col-lg-3">
      <div class="fr-select-group">
        <label class="fr-label" for="select">
          {labels.type}
        </label>
        <select 
          class="fr-select" 
          id="select" 
          name="select"
          value={typeFilter} 
          onChange={handleTypeFilterChange}
        >
          {
            types.map(type => (
              type === "all" ?
                <option key={type} value={type}>{labels.all}</option>
                :
                <option key={type} value={type}>{ capitalizeFirstLetter(type) }</option>
            ))
          }
        </select>
      </div>
    </div>
  </div>
)