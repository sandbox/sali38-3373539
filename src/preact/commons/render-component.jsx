import { render } from 'preact';

export const renderToString = (component) => {
  const div = document.createElement('div');
  render(component, div);
  return div.innerHTML;
}