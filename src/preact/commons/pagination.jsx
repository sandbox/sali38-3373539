import { useEffect } from 'preact/hooks';

const PaginationButton = ({ onClick, disabled, moreClass, children }) => (
  disabled ? (
    <a aria-disabled="true" class={`fr-pagination__link${moreClass}`} role="link">{children}</a>
  ) : (
    <a aria-disabled="false" onClick={(e) => { e.preventDefault(); onClick(); }} class={`fr-pagination__link${moreClass}`} role="link" href="#">{children}</a>
  )
);

const PaginationNumber = ({ page, currentPage, onClick }) => (
  <li>
    <a
      class={`fr-pagination__link${page === currentPage ? ' fr-pagination__link--active' : ''}`}
      aria-current={page === currentPage ? "page" : undefined}
      title={`Page ${page}`}
      href="#"
      onClick={(e) => { e.preventDefault(); onClick(page); }}
    >
      {page}
    </a>
  </li>
);

export const Pagination = ({
  nameFilter,
  typeFilter,
  currentPage,
  setCurrentPage,
  hasResults,
  totalPages,
  labels
}) => { 

  if( totalPages <= 1 ) {

    return null;

  } else {

    useEffect(() => {
      setCurrentPage(1);
    }, [nameFilter, typeFilter]);
  
    // ------------------------------------------------------------------------------ //
    // Handle arrows  
  
    const nextPage = () => {
      setCurrentPage(prev => Math.min(prev + 1, totalPages));
      return false;
    };
  
    const prevPage = () => {
      setCurrentPage(prev => Math.max(prev - 1, 1));
      return false;
    };
  
    // ------------------------------------------------------------------------------ //
    // Handle page numbers
  
    const generatePageNumbers = (currentPage, totalPages) => {
      const pageNumbers = [1];
      const pagesToShow = 5;
      const pageGroup = 3;
    
      const addPageRange = (start, end) => {
        for (let i = start; i <= end; i++) {
          pageNumbers.push(i);
        }
      };
    
      const ellipsis = () => {
        pageNumbers.push('...');
      };
    
      if (totalPages <= pagesToShow) {
        addPageRange(2, totalPages);

      } else {

        // Start (from 2 to 4)
        if (currentPage <= 2) {
          addPageRange(2, pageGroup);
          ellipsis();
        } else if (currentPage == pageGroup) {
          addPageRange(2, pageGroup + 1);
          ellipsis();
        } else if (currentPage == pageGroup + 1) {
          addPageRange(2, currentPage + 1);
          ellipsis();
  
        // End
        } else if (currentPage == totalPages) {
          ellipsis();
          addPageRange(totalPages - 2, currentPage - 1);
        } else if (currentPage >= totalPages - pageGroup) {
          ellipsis();
          addPageRange(currentPage - 1, totalPages - 1);
        
        // Middle
        } else {
          ellipsis();
          addPageRange(currentPage - 1, currentPage + 1);
          ellipsis();
        }
    
        pageNumbers.push(totalPages);
      }
    
      return pageNumbers;
    };
  
    const pageNumbers = generatePageNumbers(currentPage, totalPages);
  
    const goToPage = (page) => {
      setCurrentPage(page);
    };
    
    return (
  
      <nav role="navigation" class="fr-pagination fr-mt-5v" aria-label="Pagination">    
        <ul class="fr-pagination__list" style="justify-content:center">   
          <li>            
            <PaginationButton 
              onClick={prevPage} 
              disabled={!hasResults || currentPage === 1} 
              moreClass=' fr-pagination__link--prev fr-pagination__link--lg-label'
            >
              {labels.previous} 
            </PaginationButton>
          </li>        
          {pageNumbers.map((page, index) => 
            page === '...' 
              ? <li key={`ellipsis-${index}`}><span class="fr-pagination__link">...</span></li>
              : <PaginationNumber key={page} page={page} currentPage={currentPage} onClick={goToPage} />
          )}
          <li>            
            <PaginationButton 
              onClick={nextPage} 
              disabled={!hasResults || currentPage === totalPages} 
              moreClass=' fr-pagination__link--next fr-pagination__link--lg-label'
            >
              {labels.next}
            </PaginationButton>
          </li>         
        </ul>        
      </nav>  
    )    
  }
} 