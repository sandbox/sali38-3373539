<?php

namespace Drupal\dsfr_core;

use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\ckeditor5\Plugin\CKEditor5Plugin\Heading;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides filters format and associated CKEditor.
 */
class FilterEditor extends ControllerBase {

  /**
   * Creates a new filter format and associated CKEditor.
   *
   * @param string $format_name
   *   The machine name of the format.
   * @param string $format_label
   *   The human-readable label of the format.
   * @param string $allowed_html
   *   The allowed HTML tags for the format.
   * @param array $roles
   *   An array of role names that can use this format.
   * @param array $filters
   *   An array of filter settings.
   * @param array $toolbar_items
   *   An array of toolbar items for the editor.
   *
   * @return void
   */
  public function createFilterEditor(
    string $format_name,
    string $format_label,
    string $allowed_html = '<br> <p> <span> <mark> <strong> <em> <u> <code> <s> <sub> <sup> <a href> <ul> <ol> <li>',
    array $roles = ['authenticated', 'content_editor'],
    array $filters = [],
    array $toolbar_items = []
  ): void {

    if (empty($filters)) {

      $filters = [
          'filter_html' => [
            'status' => 1,
            'settings' => [
              'allowed_html' => $allowed_html,
            ],
          ],
          'filter_url' => [ 'status' => 1 ],
          'filter_htmlcorrector' => [ 'status' => 1 ]
      ];
    }

    /* Allowed filters 
    
    - editor_file_reference, 
    - filter_html_image_secure, 
    - filter_url, 
    - filter_html_escape, 
    - filter_caption,
    - filter_align, 
    - filter_null, 
    - filter_htmlcorrector, 
    - filter_html, 
    - filter_autop
    */

    // Create a new text format.
    $format = FilterFormat::create([
      'format' => $format_name,
      'name' => $format_label,
      'filters' => $filters,
      'roles' => $roles
    ]);
    $format->save();

    /* CKeditor 5 buttons available:
    {
      "undo":{"label":"Undo"},
      "redo":{"label":"Redo"},
      "heading":{"label":"Heading"},
      "style":{"label":"Style"},
      specialCharacters":{"label":"Special characters"},
      "sourceEditing":{"label":"Source"},
      "bold":{"label":"Bold"},
      "italic":{"label":"Italic"},
      "underline":{"label":"Underline"},
      "code":{"label":"Code"},
      "codeBlock":{"label":"Code Block"},
      "strikethrough":{"label":"Strikethrough"},
      "subscript":{"label":"Subscript"},
      "superscript":{"label":"Superscript"},
      "blockQuote":{"label":"Block quote"},
      "link":{"label":"Link"},
      "bulletedList":{"label":"Bulleted list"},
      "numberedList":{"label":"Numbered list"},
      "horizontalLine":{"label":"Horizontal line"},
      "alignment":{"label":"Text alignment"},
      "removeFormat":{"label":"Remove Format"},
      "insertTable":{"label":"table"},
      "drupalInsertImage":{"label":"Image"},
      "indent":{"label":"Indent"},
      "outdent":{"label":"Outdent"},
      "textPartLanguage":{"label":"Language"}
    }
    */

    if( count($toolbar_items) == 0 ) {

      $toolbar_items = [

        'undo',
        'redo',
        //'heading',
        'link',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'superscript',
        'subscript',
        'bulletedList', 
        'numberedList'
      ];

      $d10 =  [
        'items' => $toolbar_items,
        'plugins' => [
          'ckeditor5_heading' => Heading::DEFAULT_CONFIGURATION,
        ]
      ];

      // See: https://www.drupal.org/project/drupal/issues/3374544#comment-15173962
      $d9 = [
        'rows' => [
          [
            'name' => 'Formatting',
            'items' =>  ['Bold', 'Italic', 'Underline', 'Strikethrough', 'Superscript', 'Subscript']
          ],
          [
            'name' => 'Linking',
            'items' =>  ['DrupalLink', 'DrupalUnlink']
          ],
          [
            'name' => 'Lists',
            'items' =>  ['BulletedList', 'NumberedList']
          ],
        ]
        /*-
            name: Media
            items:
              - Blockquote
              - DrupalImage
          -
            name: Tools
            items:
              - Source
        */
      ];
    }

     /** @var \Drupal\dsfr_core\Tools $tools */
    $tools = \Drupal::service('dsfr_core.tools');
    $toolbar_drupal = ( $tools->checkDrupalVersion() == '9' ) ? $d9 : $d10;

    // Configure CKEditor for this text format.
    $editor = Editor::create([
      'format' => $format_name,
      'editor' => 'ckeditor5',
      'settings' => $toolbar_drupal
    ]);
    $editor->save();
  }
}