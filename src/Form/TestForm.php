<?php

namespace Drupal\dsfr_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['simple_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Simple Text'),
      '#required' => TRUE,
      '#error_message' => 'Empty',
    ];

    $form['search'] = [
      '#type' => 'search',
      '#title' => $this->t('Search'),
    ];

    $form['textarea'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text Area'),
    ];

    // Ajout de champs checkboxes
    $form['interests'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Interests'),
      '#options' => [
        'sports' => $this->t('Sports'),
        'music' => $this->t('Music'),
        'reading' => $this->t('Reading'),
        'travel' => $this->t('Travel'),
      ],
      '#description' => $this->t('Select your interests.'),
    ];

    $form['select'] = [
      '#type' => 'select',
      '#title' => $this->t('Select'),
      '#options' => [
        'choice1' => $this->t('Choice 1'),
        'choice2' => $this->t('Choice 2'),
        'choice3' => $this->t('Choice 3'),
      ],
    ];

    $form['radio'] = [
      '#type' => 'radios',
      '#title' => $this->t('Radio Options'),
      '#options' => [
        'option1' => $this->t('Option 1'),
        'option2' => $this->t('Option 2'),
        'option3' => $this->t('Option 3'),
      ],
      '#description' => 'Select...'
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['fr-btn--secondary'],
      ],
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Traitez les données du formulaire ici
    \Drupal::messenger()->addMessage($this->t('Form submitted successfully.'));
  }
}